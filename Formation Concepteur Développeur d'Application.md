projet : Flashcards (Carte Mémoire)

Outils : Symfony + React

Objectif : Pouvoir créer ses propres flashcards avec possibilité d'entrainement pour les apprendre

Fonctionnalité : 
- Connexion/inscription
- Créer des flashcards uniques ou les regrouper dans des dossiers
- Possibilité de tri selon la date (pour créer des entrainements du jour)
- Possibilité de créer des groupes avec d'autres utilisateurs (et d'ajouter des dossiers de flashcards dans ce groupe)
- Possibilité d'annoter des flashcards en "su" pour éviter qu'elles reviennent lors des entrainements (ou moins souvent)
- Possibilité de rechercher des dossiers de flashcards créer en public (par leur nom) et de les intégrer dans notre environnement
- 2 entrainements possibles :
	 - classique : une carte mentale apparait, on peut appuyer pour la retourner et on slide pour passer à la suivante (à droite si on la connait et à gauche dans le cas contraire) -> Paramètre : Mélanger et orientation des cartes (recto ou verso) -> A la fin possibilité de revenir sur les cartes non connues
	 - quizz : Une définition avec proposition de 4 réponses (réponses piochées dans les cartes mentales présentes dans le dossier)

![Getting Started](useCase.jpg)
![Getting Started](useCase1.jpg)
![Getting Started](mcd.jpg)

